# DESCRIPTION #

Putting all the Game date into a structure and building a library around it. 

|Version|Status|
|:--|:-:|
|Linux|**DONE**|
|Windows|**DONE**| 

# INSTRUCTIONS #

At present I'm maintaining a version for Command Line Linux and Visual Studio 2019 only, if other versions are required let me know and I'll set them up.

The build environment on Windows takes a bit of setting up if you want to do it all yourself, however, I will provide a video detailing the setup process for anyone who wants to setup their project from scratch. 

## Visual Studio 2019 ##

1. Open the project
2. Build
3. Debug
   
There should be no additional steps, a post-build process will copy the required dll files to the output directory and the debugger is set to run from there.

## Linux ##

Make sure you have the these packages installed:
```
sdl2
sdl2-devel
sdl2_image
sdl2_image-devel
```

Package names may vary on other flavours of Linux (these are the Fedora ones). 

By this stage I'd lost patience with the command line compilation and added makefile, this one is fairly general compiling all .cpp files into .o files and combining them into a single binary. Please have a look at the file if you need debugging symbols, the flag is there just commented out. 

1. Open a terminal in the Linux folder (or via MS Code)
2. Compile ```make```
3. Run ```./game```

# CREDITS #
* **Dorf** sprite -  from [Open Game Art][link](https://opengameart.org/content/dwarf) - credit Stephen Challener ([Redshrike](https://opengameart.org/users/redshrike))
* **Background** 
    - Grass by me. 
    - Tree - from [Open Game Art](https://opengameart.org/content/isometric-64x64-outside-tileset) - credit [Yar](https://opengameart.org/users/yar)
* **Walker** sprite sheets from [Open Game Art](https://opengameart.org/content/mechs-64x64) - credit [Skorpio](https://opengameart.org/users/skorpio)
* **Undead King** sprite sheet from [Open Game Art](https://opengameart.org/content/undead-king) - credit [Remax](https://opengameart.org/users/reemax)
